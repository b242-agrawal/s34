const express = require("express");

const app = express();

const port = 3000;

app.use(express.json());

app.use(express.urlencoded({extended:true}));

app.get("/", (req, res) => {
	res.send("Hello world!");
})

app.get("/home", (req, res) => {
	res.send("Welcome to the home page")
})

app.get("/hello", (req, res) => {
	res.send("Hello from /hello endpoint");
})

app.post("/hello", (req,res) => {
	res.send(`Hello there ${req.body.firstName} ${req.body.lastName}!`);
})

let users = [];

app.get("/users", (req, res) => {
	res.send(users);
})

app.post("/signup", (req,res) => {
	console.log(req.body);

	if (req.body.username !== '' && req.body.password !== '') {
		users.push(req.body);

		res.send(`User ${req.body.username} successfully registered!`)
	}
	else{
		res.send("Please input Both username and password");
	}
})

app.put("/change-password", (req,res) => {
	let message;
	for(let i=0; i<users.length; i++){
		if(req.body.username == users[i].username){
			users[i].password = req.body.password;

			message = `User ${req.body.username}'s password has been updated`;
			break;
		}
	}
	res.send(message)
})

app.delete("/delete-user", (req, res) => {
	for(let i=0; i<users.length; i++){
		if(req.body.username == users[i].username){
			users.splice(i,1);

			message = `User ${req.body.username} is removed`;
			break;
		}
	}
	res.end(message);
})

app.listen(port, () => console.log(`Server running at port ${port}`));